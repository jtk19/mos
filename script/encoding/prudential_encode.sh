#!/bin/bash

export pwd=`pwd`

export dat=/efs/shared
#export pwav=/efs/shared/prudential_segments
# For full dataset
export pwav=/efs/shared/fingerprints_larger_set

# After research, we are not using 12MFCC_0_D_A 
#export pmfcc=/efs/shared/prudential_enc/mfcc12					
#export scp=${pwd}/prudential_mfcc12.scp
#export cfg=${pwd}/htk_mfcc.cfg

# 24MFCC_E_D
#export scp1=${pwd}/prudential_mfcc24.scp
#export pmfcc1=/efs/shared/prudential_enc/mfcc24	
# For the full dataset
export scp1=${pwd}/prudential_mfcc24f.scp
export pmfcc1=/efs/shared/prudential_enc/mfcc24f	
export cfg1=${pwd}/htk_mfcc_e.cfg

sudo mkdir -p ${pmfcc1}
sudo chown -R ${USER}:${USER} ${pmfcc1}

cd ${pwav}
#find | fgrep ".wav"| sed -s "s/\.wav//; s/\.\/fingerprint//;"| \
#	xargs -I % echo ${pwav}/fingerprint%.wav ${pmfcc}/segment%.mfcc  > ${scp}
#cat ${scp} | sed -s "s/mfcc12/mfcc24/;" > ${scp1}
# For the full Prudential dataset
find | fgrep ".wav"| sed -s "s/\.wav//; s/\.\/fingerprint//;"| \
	xargs -I % echo ${pwav}/fingerprint%.wav ${pmfcc1}/segment%.mfcc  > ${scp1}

#echo "Running HCopy for 12MFCC_0_D_A feature extraction"
#HCopy -S ${scp} -T 3 -C $cfg
echo

echo "Running HCopy for 24MFCC_E_D feature extraction"
HCopy -S ${scp1} -T 3 -C ${cfg1}
echo


