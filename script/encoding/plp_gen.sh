#!/bin/bash

export f_plp='feacalc -sr 16000 -nyq 8000 -delta 2  -ras no -dom cep -plp 12 -opf htk '

find /home/data/audio/LibriSpeech/dev-other | fgrep ".wav"| sed -s "s/\.wav//"| xargs -I {} ${f_plp} {}.wav -o {}.plp

mkdir -p /home/data/audio/plp/dev
find /home/data/audio/LibriSpeech/dev-other/| fgrep ".plp"| xargs -I {} mv {} /home/data/audio/plp/dev


find /home/data/audio/LibriSpeech/test-other | fgrep ".wav"| sed -s "s/\.wav//"| xargs -I {} ${f_plp} {}.wav -o {}.plp

mkdir -p /home/data/audio/plp/test
find /home/data/audio/LibriSpeech/test-other/| fgrep ".plp"| xargs -I {} mv {} /home/data/audio/plp/test
