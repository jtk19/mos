#include <fstream>

#include "string_util.h"
#include "config.h"


using namespace std;


MOS_Prudential_Config::MOS_Prudential_Config( string cfgFile )
{

	string var, value;
	char line[1024];
	size_t i;

	ifstream cfgs( cfgFile.c_str() );
	if (cfgs.fail())
	{
		cerr << "[MOS_Prudential_Config()] Failed to open config file '"<< cfgFile.c_str()
			 << "'. Make sure the config file exists with read permissions." << endl;
		string err = string("[MOS_Prudential_Config()] Failed to open config file '") + cfgFile.c_str();
		throw invalid_argument( err );
	}

	while (cfgs.good())
	{
		cfgs.getline(line, 1023);
		string lnstr( common::trim(line) );

		if  (  ( lnstr[0] == '#' ) )		// skip as comments
		{
			continue;
		}

		i = lnstr.find('=');
		var = lnstr.substr(0, i);
		value = lnstr.substr(i + 1);
		value = common::trim(value);

		if (var.find("var1") != string::npos)
		{
		}

		if (var.find("var2") != string::npos)
		{
		}

	}
}

