#!/usr/bin/python3

import os
import subprocess
import random
import numpy as np


num_training_users = 60
MAX_TRAIN_FRAMES = 6000000

user=os.environ['USER']

dat = './'
if user == 'ubuntu' :
	print( 'Running on AWS.' )
	dat = '/efs/shared/oxford_voxceleb/voxceleb1/'
	lbld = dat + 'extract/'
elif user == 'jenni' :
	print( 'Running on Thaxila\'s laptop.' )
	dat = '/home/data/audio/oxford_voxceleb/voxceleb1/'
	lbld = dat + 'info/'
else:
	print( 'Currently supported in AWS. Please set directories in script.' )
	exit(-1)
mfccd = dat + 'mfcc/'
ed = dat + 'energy/'
svmd = dat + 'svm/'
svm_malef = svmd + 'male.dat'
svm_femalef = svmd + 'female.dat'
matlabf = svmd + 'features_mffc40_f.dat'
matlabm = svmd + 'features_mffc40_m.dat'
print( 'Label files in: ' + lbld )
print( 'writing data to: ')
print( '    ' + svm_malef )
print( '    ' + svm_femalef )

mlabels = []
flabels = []
mnameset = set()
fnameset = set()

mf = lbld + 'vox1_male_id_100_data.csv'
ff = lbld + 'vox1_female_id_100_data.csv'
male_f = open( mf )
female_f = open( ff )

svm_malef = open( svm_malef, 'w' );
svm_femalef = open( svm_femalef, 'w' );
matlabf_dat = open( matlabf, 'w' );
matlabm_dat = open( matlabm, 'w' );

print( '\nProcessing :' + mf )
for line in male_f:
	lbl = line.strip().split(' ')
	mlabels.append( lbl )
	mnameset.add( lbl[1] )
print( mnameset )
print()

print( 'Processing :' + ff )
for line in female_f:
	lbl = line.strip().split(' ')
	flabels.append( lbl )
	fnameset.add( lbl[1] )
print( fnameset )
print()

names = list( mnameset )
names += list( fnameset )
mnameset.clear()
fnameset.clear()
	
np.set_printoptions( precision = 8 )
np.set_printoptions(suppress=True)

segid = 0
framecount = 0

N = len(mlabels)
for i in range(N):
	# -----------------------------------------------------
	# Generaiton of data for a male speaker segment
	f = mfccd + mlabels[i][1] + '/' + mlabels[i][2]
	f1 = ed + mlabels[i][1] + '/' + mlabels[i][2]
	uid = str( names.index( mlabels[i][1]) )
	segid += 1
	print( f )
	hlist = subprocess.run([ 'HList', '-r', f], stdout=subprocess.PIPE)
	hlist1 = subprocess.run([ 'HList', '-r', f1], stdout=subprocess.PIPE)
	data = hlist.stdout.decode('utf-8').split('\n'); 
	data1 = hlist1.stdout.decode('utf-8').split('\n'); 
	for d, d1 in zip( data, data1):
		if  d.strip(): 
			framecount += 1
			rec = np.asfarray( d.strip().split(' '), float )
			rec1 = np.asfarray( d1.strip().split(' '), float )
			np.append( rec, rec1[12] )
			s1 = '-1 ';			# males classified as -1
			s2 = '-1 ' + mlabels[i][1] + ' '
			s2 += (mlabels[i][2]).replace( '.mfcc', '' ) + ' '
			for i in range(39):
				s1 += "%d:%f " % ( i+1, rec[i] )
				s2 += "%f " % ( rec[i] )
			s1 += "%d:%f" % ( 42, rec1[12] )
			s2 += "%f\n" % ( rec1[12] )
			s1 += ' # ' + mlabels[i][1]
			s1 += ' ' + (mlabels[i][2]).replace( '.mfcc', '' )
			s1 += '\n'
			#print( s1)
			svm_malef.write( s1 )
			matlabm_dat.write( s2 )
		# end-if
	# end-for
#end-for-N

N = len(flabels)
for i in range(N):
	# -----------------------------------------------------
	# Generaiton of data for a female speaker segment
	f = mfccd + flabels[i][1] + '/' + flabels[i][2]
	f1 = ed + flabels[i][1] + '/' + flabels[i][2]
	uid = str( names.index( flabels[i][1]) )
	segid += 1
	print( f )
	hlist = subprocess.run([ 'HList', '-r', f], stdout=subprocess.PIPE)
	hlist1 = subprocess.run([ 'HList', '-r', f1], stdout=subprocess.PIPE)
	data = hlist.stdout.decode('utf-8').split('\n'); 
	data1 = hlist1.stdout.decode('utf-8').split('\n'); 
	for d, d1 in zip( data, data1):
		if  d.strip(): 
			framecount += 1
			rec = np.asfarray( d.strip().split(' '), float )
			rec1 = np.asfarray( d1.strip().split(' '), float )
			np.append( rec, rec1[12] )
			s1 = '1 ';				# females classified as +1
			#s1 += '1:' + uid + ' 2:' + str(segid) + ' '
			s2 = '1 ' + flabels[i][1] + ' '
			s2 += (flabels[i][2]).replace( '.mfcc', '' ) + ' '
			for i in range(39):
				s1 += "%d:%f " % ( i+1, rec[i] )
				s2 += "%f " % ( rec[i] )
			s1 += "%d:%f" % ( 42, rec1[12] )
			s2 += "%f\n" % ( rec1[12] )
			s1 += ' # ' + flabels[i][1]
			s1 += ' ' + (flabels[i][2]).replace( '.mfcc', '' )
			s1 += '\n'
			#print( s1)
			svm_femalef.write( s1 )
			matlabf_dat.write( s2 )
		# end-if
	# end-for
# end-for-N

		
male_f.close()
female_f.close()
svm_malef.close()
svm_femalef.close()
matlabm_dat.close()
matlabf_dat.close()

