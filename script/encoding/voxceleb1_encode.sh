#!/bin/bash


export pwd=`pwd`

if [[ $USER == "ubuntu" ]];									## on AWS
then
	echo "Running on AWS for user: ${USER}"
	export dat=/efs/shared/oxford_voxceleb/voxceleb1
elif [[ $HOSTNAME == "watson.texelengineering.co.uk" ]]		## on Jenni's laptop
then
	##echo "Not supported on Thaxila's Fedora machine; only supported on AWS."
	##exit -2;
	echo "Running onn Thaxila's Fedora machine."
	export dat=/home/data/audio/oxford_voxceleb/voxceleb1
else											## on a Ubuntu for Windows installation
	echo "Not supported on Windows Ubuntu; only supported on AWS."
	exit -3;
	export dat=/mnt/c/data/oxford_voxceleb/voxceleb1
	mkdir -p ${dat}
fi

export pwav=${dat}/voxceleb1_wav
export pmfcc=${dat}/mfcc
export penergy=${dat}/energy
export scp=${pwd}/voxceleb1_mfcc.scp
export scp_e=${pwd}/voxceleb1_energy.scp
export cfg=${pwd}/htk_mfcc.cfg
export cfg_e=${pwd}/htk_mfcc_e.cfg

sudo mkdir -p ${pmfcc} ${penergy}
sudo chown -R ${USER}:${USER} ${pmfcc} ${penergy}


#------- main [ MFFC, C0 ] with first and second derivatives ---------------------------
echo ${pwav}
cd ${pwav}
echo "creating speaker mfcc and energy sub-directories..."
find | fgrep ".wav"| sed -s "s/.*\/\(.*\)\/.*/\1/p"| xargs -I {} mkdir -p ${pmfcc}/{}/ ${penergy}/{}/
#find | fgrep ".wav"| sed -s "s/.*\/\(.*\)\/.*/\1/p"| xargs -I {} mkdir -p ${penergy}/{}/

#echo "creating feature extraction scripts for main MFFC ..."
find | fgrep ".wav"| sed -s "s/\.wav//; s/\.\///"| \
	xargs -I % echo "${pwav}/%.wav ${pmfcc}/%.mfcc" > ${scp}
	
#echo "Running HCopy for MFCC_0_D_A feature extraction"
HCopy -S ${scp} -T 3 -C $cfg

#------- Energy feature ---------------------------
cd ${pwav}

echo "creating feature extraction scripts for Energy ..."
cat ${scp} | sed -s "s/\/mfcc/\/energy/" > ${scp_e}
	
echo "Running HCopy for MFCC24_E_D feature extraction"
HCopy -S ${scp_e} -T 3 -C ${cfg_e}


echo "MFCC feature extraction for voxceleb1 complete in: ${pmfcc}"
echo


## ------- Label processing for gender and speaker ID ----------------------------

export genders=${dat}/info/voxceleb1_genders.txt
export identities=${dat}/info/voxceleb1_identity.txt

export female_id_dat=${dat}/info/vox1_female_id_data.csv
export male_id_dat=${dat}/info/vox1_male_id_data.csv

echo "Processing VoxCeleb Data in ${pwav}"

fgrep "female" ${genders} | sed -s "s/ female//" | xargs -I {} fgrep {} ${identities} \
	| sed -s "s/\//\ /;s/wav/mfcc/" > ${female_id_dat}
echo

fgrep -v "female" ${genders} | sed -s "s/ male//" | xargs -I {} fgrep {} ${identities} \
	| sed -s "s/\//\ /;s/wav/mfcc/" > ${male_id_dat}
echo

echo "Gender and ID labels written to:"
echo "  $female_id_dat"
echo "  $male_id_dat"
echo
