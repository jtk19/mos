#ifndef __MOS_PRUDENTIAL_CONFIG_H__
#define __MOS_PRUDENTIAL_CONFIG_H__

#include <iostream>
#include <vector>

#define MOS_PRUDENTIAL_CONFIG_FILE		"mos_prudential.cfg"


using namespace std;


class MOS_Prudential_Config
{
  public:
	MOS_Prudential_Config( string cfgFile = MOS_PRUDENTIAL_CONFIG_FILE );
	~MOS_Prudential_Config() = default;


  public:

};

#endif
