#!/bin/bash

argc=$#

if [ $argc -lt 1 ]
then 
	echo "usage: " ${0} " <maximum number of male/female speakers>"
	exit -1
fi

export pwd=`pwd`

if [[ $USER == "ubuntu" ]];									## on AWS
then
	export dat=/efs/shared/oxford_voxceleb/voxceleb1
elif [[ $HOSTNAME == "watson.texelengineering.co.uk" ]]		## on Jenni's laptop
then
	echo "Not supported on Jenni's Fedora machine; only supported on AWS."
	exit -2;
	export dat=/home/data/audio/oxford_voxceleb/voxceleb1
else														## on a Ubuntu for Windows installation
	echo "Not supported on Windows Ubuntu; only supported on AWS."
	exit -3;
	export dat=/mnt/c/data/oxford_voxceleb/voxceleb1
	mkdir -p ${dat}
fi

export pwav=${dat}/voxceleb1_wav
#export pmfcc=${dat}/mfcc
export pmfcc=${dat}/energy
export scp=${pwd}/voxceleb1_mfcc.scp
export cfg=${pwd}/htk_mfcc.cfg

export genders=${dat}/info/voxceleb1_genders.txt
export identities=${dat}/info/voxceleb1_identity.txt

export female_id_dat=${dat}/extract/vox1_female_id_${1}_data.csv
export male_id_dat=${dat}/extract/vox1_male_id_${1}_data.csv

echo "Processing VoxCeleb Data in ${pwav}"

fgrep "female" ${genders} | head -${1} | sed -s "s/ female//" | xargs -I {} fgrep {} ${identities} \
	| sed -s "s/\//\ /;s/wav/mfcc/" > ${female_id_dat}
echo

fgrep -v "female" ${genders} | head -${1} | sed -s "s/ male//" | xargs -I {} fgrep {} ${identities} \
	| sed -s "s/\//\ /;s/wav/mfcc/" > ${male_id_dat}
echo

echo "Gender and ID labels written to:"
echo "  $female_id_dat"
echo "  $male_id_dat"
echo

tar=${dat}/extract/vox1_extract_${1}.tar
rm -rf $tar

cd ${pmfcc}
echo "Moved to: " `pwd`
echo "Archiving females' speech... tar --append -f " ${tar} "./<speaker>/*"
cat $female_id_dat | sed -s "s/^.* \(.*\) .*$/\.\/\1\//"| uniq \
	| xargs -I {} tar --append --recursion -f ${tar} {}
	
echo
echo "Moved to: " `pwd`
echo "Archiving males' speech... tar --append -f " ${tar} "./<speaker>/*"
cat $male_id_dat | sed -s "s/^.* \(.*\) .*$/\.\/\1\//"| uniq \
	| xargs -I {} tar --append --recursion -f ${tar} {} 

##n=`cat $female_id_dat| wc -l`
##sed -i '1i'$n'' $female_id_dat
##n=`cat $male_id_dat| wc -l`
##sed -i '1i'$n'' $male_id_dat

cd ${dat}/extract
tar --append --recursion -f ${tar} ./vox1*_${1}_data.csv

echo
echo "Compressing..."
gzip -9 -f ${tar} > ${tar}.gz
echo "MFCC data subset extracted to:  ${tar}.gz"

